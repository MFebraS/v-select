This is a custom select option with dropdown and search options.

# Dependencies
    - jQuery
    - Bootstrap css

# Feature
    - Custom select
    - Search option
    - Enable render with form repeater
