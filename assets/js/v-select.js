$.fn.vselect = function(config={}) {
	$(this).each(function(index, select_elm) {
		if ($(select_elm).is('select')) {
			$(select_elm).hide();
			let value = $(select_elm).val();
			let value_str = '';
			let selected = false;

			// Get option value and text
			let data = [];
			let options = '';
			$(select_elm).find('option').each(function(index, item) {
				data.push({
					id: item.value,
					text: item.text
				});

				if ($(item).attr('selected') !== undefined) {
					options += '<li data-id="'+ item.value +'" class="active">'+ item.text +'</li>';
					value_str = item.text;
					selected = true;
				}
				else {
					options += '<li data-id="'+ item.value +'">'+ item.text +'</li>';
				}
			});

			if (!selected && config.placeholder) {
				value_str = '<span class="v-select-placeholder">'+ config.placeholder +'</span>';
			}

			let v_select = '<div class="v-select-container">' +
				'<div class="v-select-value-wrapper form-control d-flex align-items-center">' +
					'<span class="v-select-value">'+ value_str +'</span>' +
					'<span class="v-select-icon"></span>' +
				'</div>' +
				
				'<div class="v-select-dropdown card d-none">' +
					'<div class="v-select-search-wrapper">' +
						'<input class="v-select-search form-control" type="text">' +
					'</div>' +
					'<ul class="v-select-option-wrapper">' +
						options +
					'</ul>' +
				'</div>' +
			'</div>';

			$(select_elm).after(v_select);
		}
	});
};

// Toggle dropdown
$(document).on('click', '.v-select-value-wrapper', function() {
	let container = $(this).parents('.v-select-container');

	if ($(this).next().hasClass('d-none')) {
		// Close another v-select
		$('.v-select-container').not(container).each(function(index, item) {
			vSelectClose($(item));
		});

		$(this).next().removeClass('d-none');
		$(this).next().find('.v-select-search').val('').focus();
	}
	else {
		vSelectClose(container);
	}
});

// Close dropdown when click outside
$(document).on('click', function(e) {
	let container = $('.v-select-container');
	if (!container.is(e.target) && container.has(e.target).length === 0) {
		vSelectClose(container);
	}
});

// Search
$(document).on('keyup', '.v-select-search', function() {
	let container = $(this).parents('.v-select-container');
	let select_elm = container.prev();
	let selected_id = select_elm.val();
	let data = vSelectGetData(select_elm);

	let keyword = $(this).val();
	let condition = new RegExp(keyword, 'i');
	let result = data.filter(function(item) {
		return condition.test(item.text);
	});

	vSelectSetOption(result, container.find('.v-select-option-wrapper'), selected_id);
});

// Select Value
$(document).on('click', '.v-select-option-wrapper li', function() {
	let id = $(this).data('id');
	if (id !== '#') {
		let text = $(this).text();
		let container = $(this).parents('.v-select-container');
		container.find('.v-select-value').text(text);
		container.prev().val(id);
		container.prev().find('option').removeAttr('selected');
		container.prev().find('option[value="'+ id +'"]').attr('selected', true);

		vSelectClose(container);
	}
});

// Close dropdown
function vSelectClose(container) {
	container.each(function(index, element) {
		$(element).find('.v-select-dropdown').addClass('d-none');
		vSelectResetOption($(element));
	});
}

// Get options data
function vSelectGetData(select_elm, check_selected=false) {
	let data = [];
	let options = '';
	let selected = false;

	select_elm.find('option').each(function(index, item) {
		data.push({
			id: item.value,
			text: item.text
		});

		if ($(item).attr('selected') !== undefined) {
			selected = true;
		}
	});

	if (check_selected) {
		return {
			data: data,
			selected: selected
		};
	}

	return data;
}

// Set options
function vSelectSetOption(data, option_wrapper_elm, id='') {
	let options = '';
	if (Array.isArray(data) && data.length > 0) {
		data.forEach( function(item) {
			if (id && id === item.id.toString()) {
				options += '<li data-id="'+ item.id +'" class="active">'+ item.text +'</li>';
			}
			else {
				options += '<li data-id="'+ item.id +'">'+ item.text +'</li>';
			}
		});
	}
	else {
		options = '<li data-id="#">No option</li>';
	}

	option_wrapper_elm.html(options);
}

// Reset options
function vSelectResetOption(container) {
	let select_elm = container.prev();
	let result = vSelectGetData(select_elm, true);
	let selected_id = '';
	if (result.selected) {
		selected_id = select_elm.val();
	}
	vSelectSetOption(result.data, container.find('.v-select-option-wrapper'), selected_id);
}
